package com.hendisantika.dto.request;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-enum-example
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/5/22
 * Time: 21:16
 * To change this template use File | Settings | File Templates.
 */
public enum GenreRequest {
    SCI_FI, FANTASY, COMEDY, DRAMA;

    @JsonCreator
    public static GenreRequest create(String value) {
        return GenreRequest.valueOf(value.toUpperCase());
    }
}
