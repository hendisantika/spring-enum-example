package com.hendisantika.dto.request;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-enum-example
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/5/22
 * Time: 21:15
 * To change this template use File | Settings | File Templates.
 */
@Data
public class CreateBookRequest {
    private String title;
    private GenreRequest genre;
}
