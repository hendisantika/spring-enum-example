package com.hendisantika.dto.response;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-enum-example
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/5/22
 * Time: 21:18
 * To change this template use File | Settings | File Templates.
 */
@Data
public class BookResponse {
    private String title;
    private GenreResponse genre;

    public static class Builder {

        private final BookResponse book;

        public Builder() {
            book = new BookResponse();
        }

        public Builder title(String value) {
            book.setTitle(value);
            return this;
        }

        public Builder genre(String value) {
            book.setGenre(GenreResponse.valueOf(value));
            return this;
        }

        public BookResponse build() {
            return book;
        }

    }
}
