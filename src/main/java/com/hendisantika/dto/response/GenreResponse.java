package com.hendisantika.dto.response;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-enum-example
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/5/22
 * Time: 21:17
 * To change this template use File | Settings | File Templates.
 */
public enum GenreResponse {

    SCI_FI, FANTASY, COMEDY, DRAMA;

    @JsonValue
    public String get() {
        return this.name().toLowerCase();
    }
}
