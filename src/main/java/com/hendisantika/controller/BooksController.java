package com.hendisantika.controller;

import com.hendisantika.dto.request.CreateBookRequest;
import com.hendisantika.dto.response.BookResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-enum-example
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/5/22
 * Time: 21:20
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("/books")
@Tag(name = "/books", description = "Endpoint for list all books")
public class BooksController {

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
            summary = "Add New Book",
            description = "Add New Book."
    )
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            BookResponse.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Not Found",
                    responseCode = "404",
                    content = @Content
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Internal Error",
                    responseCode = "404",
                    content = @Content
            )
    })
    public BookResponse createBook(@RequestBody CreateBookRequest request) {
        return new BookResponse.Builder().title(request.getTitle()).genre(request.getGenre().name()).build();
    }
}
