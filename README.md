# Spring ENUM Request

### Todo list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-enum-example.git`
2. Navigate to the folder: `cd spring-enum-example`
3. Run the application: `mvn clean spring-boot:run`
4. Open your favorite browser: http://localhost:8080/swagger-ui

```shell
curl --location --request POST 'localhost:8080/books' \
--header 'Content-Type: application/json' \
--data-raw '{
  "title": "Belajar Spring Boot",
  "genre": "FANTASY"
}'

curl --location --request POST 'localhost:8080/books' \
--header 'Content-Type: application/json' \
--data-raw '{
  "title": "Belajar Spring Boot",
  "genre": "SCI-FI"
}'

curl --location --request POST 'localhost:8080/books' \
--header 'Content-Type: application/json' \
--data-raw '{
  "title": "Belajar Spring Boot",
  "genre": "COMEDY"
}'

curl --location --request POST 'localhost:8080/books' \
--header 'Content-Type: application/json' \
--data-raw '{
  "title": "Belajar Spring Boot",
  "genre": "DRAMA"
}'
```

### Image Screenshot

REQUEST SAMPLE

![REQUEST SAMPLE](img/home.png "REQUEST SAMPLE")